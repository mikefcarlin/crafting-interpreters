
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct Node Node;

struct Node {
    char * inner;
    Node *prev;
    Node *next;
};

typedef struct NodeList {
    Node *root;
    Node *end;   
} NodeList;

NodeList *initNodeList() {
   NodeList *nodeList = (NodeList*) malloc(sizeof(NodeList));
   return nodeList;
}

Node * insert(NodeList *list, char * item) {
    Node* node = (Node *) malloc(sizeof(Node));
    node->inner = strdup(item);
    node->prev = list->end;

    if (list->root == NULL) {
       list->root = node;
       list->end = node; 
    } else {
        list->end->next = node;
        list->end = node;
    }
    return node;
}

Node * find(NodeList *list, char * item) {
    Node *node = list->root;
    while (node != NULL) {
        if (strcmp(item, node->inner) == 0) {
            return node;
        }
       node = node->next; 
    }
    
    return NULL;
}

void delete(NodeList *list, Node* node) {
    // Update list meta
    if (node == list->root) {
        list->root = node->next;
    } 

    if (node == list->end) {
        list->end = node->prev;        
    } 
    
    // Update linked nodes: handles the following cases
    // [p] <--> [c] <--> [n]   =====> [p] <--> [n]
    // null <--> [c] <--> [n]  =====> null <--> [n]
    // [p] <--> [c] <--> null  =====> [p] <--> null
    // null <--> [c] <--> null =====> 

    if (node->prev != NULL) {
        node->prev->next = node->next;
    }
    
    if (node->next != NULL) {
        node->next->prev = node->prev;
    }
}

void printList(NodeList* list) {
     Node *node = list->root;
     printf("Start -> ");
     while (node != NULL) {
        printf("%s -> ", node->inner);
        node = node->next;
     }
   
     printf("End\n");
}

int main(int argc, char const *argv[])
{
    printf("Delete root example:\n");
    NodeList *list = initNodeList();
    insert(list, "pls");
    insert(list, "work");
    printList(list);

    Node *node = find(list, "pls"); 
    delete(list, node);
    printList(list);

    printf("Delete end example:\n");
    NodeList *list2 = initNodeList();
    insert(list2, "pls");
    insert(list2, "work");
    printList(list2);

    node = find(list2, "work"); 
    delete(list2, node);
    printList(list2);

    printf("Delete middle example:\n");
    NodeList *list3 = initNodeList();
    insert(list3, "pls");
    insert(list3, "work");
    insert(list3, "m8");
    printList(list3);

    node = find(list3, "work"); 
    delete(list3, node);
    printList(list3);

    return 0;
}
